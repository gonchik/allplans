${webResourceManager.requireResource("be.foreach.bamboo.allplans:allplans-resources")}

[#import "/fragments/plan/displayWideBuildPlansList.ftl" as widePlanList]
[#if allBranchPlans?has_content]
    [#compress]
    <input type="checkbox" checked="checked" id="hideNeverExecuted"><label for="hideNeverExecuted">Hide 'Never built' and 'Suspended' builds</label> -
    <input type="checkbox" id="hideUnknown"><label for="hideUnknown">Hide 'Unknown' builds</label> -
    <input type="checkbox" [#if !currentlyFilteredBranch?has_content] checked="checked" [/#if] id="hideSuccessful"><label for="hideSuccessful">Hide 'Successful' builds</label> -
    Filter branch plan: <select id="changeBranch">
        <option value="">-- ALL --</option>
        [#list branchNames as branchName]
            <option value="${branchName}" [#if currentlyFilteredBranch?has_content && branchName == currentlyFilteredBranch] selected="selected" [/#if]>${branchName}</option>
        [/#list]
    </select>
    [#list allBranchPlans?keys as key]
        [#assign projectPlans = action.getPlansForProject( key ) /]
        [#if projectPlans?has_content]
            <h1>Project: ${key.name}</h1>
            <div>
                [#list projectPlans?keys as key]
                    [#assign builds = projectPlans.get(key) />
                    [#if builds?has_content]
                        [@widePlanList.displayWideBuildPlansList builds=builds showProject=false /]
                    [/#if]
                [/#list]
            </div>
        [/#if]
    [/#list]
    [/#compress]
[#else]
    No projects to view
[/#if]